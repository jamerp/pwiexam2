import couchdb
import json
import jinja2
import os
import uuid
import random
import requests
import webapp2

from google.appengine.api import taskqueue
from google.appengine.ext import db

#############################################################################

def get_servers():
    SERVERS = {}
    servers = json.loads(requests.get('http://194.29.175.241:5984/calc1/_design/utils/_view/list_active').text)['rows']
    for server in servers:
        server = server['value']
        if server['host'][-1] == '/':
            server['host'] = server['host'][0:(len(server['host'])-1)]
        if server['calculation'][0] == '/':
            server['calculation'] = server['calculation'][1:len(server['calculation'])]
        if server['operator'] in SERVERS:
            SERVERS[server['operator']].append(server['host'] + '/' + server['calculation'])
        else:
            SERVERS[server['operator']] = [server['host'] + '/' + server['calculation']]
    return SERVERS

#############################################################################

class Result(db.Model):
    result = db.StringProperty()
    date = db.DateTimeProperty(auto_now_add=True)
    success = db.BooleanProperty()
    retries = db.IntegerProperty()

#############################################################################

class MainPage(webapp2.RequestHandler):
    def get(self):
        global SERVERS

        try:
            couchdb.Server('http://194.29.175.241:5984')['calc1'].save({
                '_id' : COUCHDB_ID,
                'host' : 'http://pwiexam.appspot.com',
                'active' : True,
                'operator' : '*',
                'calculation' : '/mul',
            })
        except:
            pass

        template = JINJA_ENVIRONMENT.get_template('index.html')
        self.response.write(template.render({
            'servers' : get_servers()
        }))

#############################################################################

class MulCounter(webapp2.RequestHandler):
    def post(self):
        try:
            data = json.loads(self.request.body)
            self.response.write(json.dumps({"id" : data['id'], "result" : data['number1'] * data['number2'], "success" : True}))
        except:
            self.response.write(json.dumps({"id" : data['id'], "result" : 0, "success" : False}))

#############################################################################

class Worker(webapp2.RequestHandler):
    def error(self, msg):
        result = Result.get_by_key_name(self.request.get('result_id'))
        result.result = msg
        result.put()

    def post(self):
        try:
            result = Result.get_by_key_name(self.request.get('result_id'))
            result.retries += 1
            if result.retries > 3:
                self.error('Error!')
                return
            SERVERS = get_servers()
            stack = []
            for op in self.request.get('expr').split(' '):
                try:
                    number = float(op)
                    stack.append(number)
                except:
                    if op in SERVERS:
                        servers = SERVERS[op]
                        server = servers[random.randrange(0, len(servers))]
                        result = json.loads(requests.post(server, data=json.dumps({
                            "id" : self.request.get('result_id'),
                            "number1" : stack.pop(),
                            "number2" : stack.pop(),
                        }), headers={'content-type' : 'application/json'}).text)
                        if result['success']:
                            stack.append(result['result'])
                        else:
                            self.error('Error!')
                            return
                    else:
                        self.error('Error!')
                        return
        except:
            self.error('Error!')
            return
        
        try:
            result = Result.get_by_key_name(self.request.get('result_id'))
            result.result = str(stack.pop())
            result.success = True
            result.put()
            return
        except:
            self.error('Error!')
            return

#############################################################################

class LastResults(webapp2.RequestHandler):
    def get(self):
        self.response.write('<table class="table table-hover table-bordered">')
        for result in Result.all().order('-date').fetch(10):
            if result.success:
                self.response.write('<tr class="success"><td>' + str(result.date) + '</td><td>' + result.result + '</td></tr>')
            elif result.result == 'In Progress...':
                self.response.write('<tr class="info"><td>' + str(result.date) + '</td><td>' + result.result + '</div></td></tr>')
            else:
				self.response.write('<tr class="error"><td>' + str(result.date) + '</td><td>' + result.result + '</div></td></tr>')
        self.response.write('</table>')

#############################################################################

class TaskCreator(webapp2.RequestHandler):
	def post(self):
		expr = self.request.get('expr', '')
		if expr != '':
			result_id = str(uuid.uuid4())
			Result(key_name=result_id, result='In Progress...', success=False, retries=0).put()
			taskqueue.add(url='/worker', params={'expr' : expr, 'result_id' : result_id})

#############################################################################

COUCHDB_ID = '310fcdc0-d7e8-11e2-a28f-0800200c9a66'

JINJA_ENVIRONMENT = jinja2.Environment(
    loader = jinja2.FileSystemLoader(os.path.dirname(__file__))
)

app = webapp2.WSGIApplication([
    ('/', MainPage),
    ('/mul', MulCounter),
    ('/worker', Worker),
    ('/results', LastResults),
    ('/send', TaskCreator),
], debug=True)
